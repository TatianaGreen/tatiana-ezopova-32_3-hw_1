# 1)создать класс SuperHero с атрибутом класса people='people'
class SuperHero:
    people = 'people'
    
# 2)создать конструктор класса(init) с атрибутами name,nickname,superpower,health_points, catchphrase.
    def __init__(self, name, nickname, superpower, health_points, catchphrase):
        self.name = name
        self.nickname = nickname
        self.superpower = superpower
        self.health_points = health_points
        self.catchphrase = catchphrase
        
# 3)создать метод который выводит имя героя
    def hero_name(self):
        return f'name is {self.name}'

# 4)создать метод который умножает здоровье героя на 2
    def health_mul(self):
        return self.health_points * 2

# 5) cоздать магический!! метод который выводит прозвище героя,его суперспособность и его здоровье
    def str(self):
        return f'nickname is {self.nickname} \n' \
               f'{self.superpower} \n' \
               f'{self.health_points}'

# 6)создать магический!! метод который считает длину коронной фразы героя
    def __len__(self):
        return len(self.catchphrase)

# 7)создать объект класса Hero и применить все методы которые вы создали
hero = SuperHero('Supermen', 'SuperHero', 'flight and strong', 5, 'I will be back!')

print(hero.health_points)
print(hero.name)
print(hero.health_mul())
print(hero.catchphrase)
print(len(hero.catchphrase))



